# Kaleidoscope

A Clojure library designed to ... well, that part is up to you.

## EmacsでClojure

clojure-mode, ciderを入れておく。

```
M-x cider-jack-in # REPL起動
M-x cider-load-file # REPLにClojureファイルをロード
M-x cider-eval-last-sexp #カーソル前のS式を評価
```


## Repl実行

```
user=>(require '[projectname.core]) ; パッケージをrequire
user=>(in-ns 'projectname.core) ; ネームスペースの内側で作業
user=>(start-server) ; サーバー起動
```

## Selmer(HTMLテンプレートエンジン)のパス設定

ProjectDir/resources/templates以下にhtmlテンプレートを配置する。
project.cljに、resource-pathの設定を追加する。

```
:resource-path "resources"
```

コード内でparser/set-resource-path!を使い、テンプレートの場所を指定する。

```
(ns kaleidoscope.core
  (:require [ring.adapter.jetty :as server]
            [selmer.parser :as parser]))

(parser/set-resource-path! (clojure.java.io/resource "template"))
(parser/render-file "index.html {:content "Hello,World"}))
```

## lein runで起動させるには

```
(defn -main []
  (start-server))
```

を記述して、以下のコマンドで起動。

```$ lein run -m kaleidoscope.core```

-mオプションを省きたい場合は、project.cljに:mainを記載する。

```
:main kaleidoscope.core/start-server
```

## Dockerで動かす

nginxとapp-serverのふたつのコンテナを構築。

```
version: '2'
services:
  app-server:
    build: "./app-server"
    container_name: "app"
    working_dir: "/usr/src/app"
    ports:
      - "3000:3000"
    volumes:
      - ".:/usr/src/app"
      - "./m2:/home/docker/.m2"
  http-server:
    image: nginx:latest
    container_name: "http"
    volumes:
      - "./nginx/conf.d/default.conf:/etc/nginx/conf.d/default.conf"
      - "./public:/usr/share/nginx/html"
    links:
      - app-server
    ports:
      - "80:80"
```

app-server/Dockerfileは以下の内容。プロジェクトディレクトリを丸ごとコピーし、lein runを実行する。
```
FROM clojure
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD ["lein", "run"]
```

docker-composeのportを3000番にしておく。

http-server(nginx)のほうは、volumesでnginxの設定ファイルと、public以下を結びつける。default.confは以下のように記述。

```
server {
    listen       80;
    server_name  localhost;

    location / {
        proxy_pass http://app-server:3000;
    }

    location ~* \.(css|js|jpg|png|gif|svg|html|htm)$ {
        root /usr/share/nginx/html;
        index index.html index.htm;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```

通常アクセスはすべてapp-serverの3000番ポートに。
HTMLで使いそうな静的ファイルは/sur/share/nginx/html以下のものを使う。
ここで使っているapp-serverという名前は、docker-compose.ymlのlinksの箇所で設定した名前を使う。エイリアスをつけておくこともできる。

```
links
  - app-server: myapp
```

## Reload

### namespaceを指定してリロード

```
$ lein repl
2017-10-23 20:44:48.321:INFO::main: Logging initialized @1823ms
nREPL server started on port 46243 on host 127.0.0.1 - nrepl://127.0.0.1:46243
REPL-y 0.3.7, nREPL 0.2.12
Clojure 1.8.0
OpenJDK 64-Bit Server VM 1.8.0_141-8u141-b15-1~deb9u1-b15
    Docs: (doc function-name-here)
          (find-doc "part-of-name-here")
  Source: (source function-name-here)
 Javadoc: (javadoc java-object-or-class-here)
    Exit: Control+D or (exit) or (quit)
 Results: Stored in vars *1, *2, *3, an exception in *e

kaleidoscope.core=> (use :reload-all '[kaleidoscope.core])
nil
```

### ブラウザリロード時にコードを再読込

ring.middleware.reloadを使い、run-jettyに渡すhandlerをwrapする形でwrap-reloadを適用する。

```
(:require [ring.middleware.reload :refer [wrap-reload]])

(defroutes handler
  (GET "/" [] home))

(def reloadable-app
  (wrap-reload #'handler))

(defn start-server []
  (when-not @server
    (reset! server (server/run-jetty reloadable-app {:port 3000 :join? false}))))
```

## React and TypeScript

```
$ npm install react react-dom react-tap-event-plugin material-ui @types/react @types/react-dom @types/material-ui @types/react-tap-event-plugin --save-dev
```

### Attention

inject-tap-event-pluginはrequireで呼び出す必要あり。
```
import injectTapEventPlugin = require("react-tap-event-plugin");
```

## Gulp

```
$ npm install gulp --save-dev
```

### sass
```
$ npm install gulp-sass
```

## Stylus

```
$ npm install gulp-stylus --save-dev
```
## fetch

```
$ npm install whatwg-fetch --save-dev
```

## Pug

```
$ npm install gulp-pug gulp-notify gulp-plumber gulp-connect
```

```
gulp.task('pug', () => {
  return gulp.src(path.src_pug)
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest(path.dest_html))
    .pipe(connect.reload())
});
```

## Heroku

```
$ sudo add-apt-repository "deb https://cli-assets.heroku.com/branches/stable/apt ./"
$ curl -L https://cli-assets.heroku.com/apt/release.key | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install heroku
```

## License

Copyright © 2017

Distributed under the MPL or any later version.
