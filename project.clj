(defproject kaleidoscope "0.1.0-SNAPSHOT"
  :description "CMS"
  :url "https://kaleidoscope.website"
  :license {:name "MPL-2.0"
            :url "https://www.mozilla.org/en-US/MPL/2.0/"}
  :main kaleidoscope.core
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [cheshire "5.8.0"]
                 [ring "1.6.2"]
                 [ring/ring-defaults "0.3.1"]
                 [selmer "1.11.1"]
                 [compojure "1.6.0"]
                 [com.novemberain/monger "3.1.0"]
                 [buddy "2.0.0"]
                 [clj-html-compressor "0.1.1"]
                 ]
  :plugins [[lein-kibit "0.1.5"]
            ]
  :resource-path "resources")
