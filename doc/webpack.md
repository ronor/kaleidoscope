# JS Frontend

Yarn

```
$ npm install -g yarnpkg webpack
$ exec $SHELL -l (ndenvを利用しているため)
$ mkdir frontend
$ cd frontend
$ yarn init
$ yarn add react react-dom
$ yarn add webpack --dev
$ yarn add babel-loader babel-core babel-preset-react babel-preset-env --dev
```

.babelrc
```
{
    "presets": [
        ["env", {
            "targets": {
                "browsers": ["last 1 versions"]
            }
        }],
        "react"
    ]
}
```

reactstrap

```
$ yarn add bootstrap@4.0.0-beta --save
$ yarn add reactstrap@next --save
```

cssをjsからimport

```
$ yarn add style-loader css-loader --dev
```

webpack.config.jsのmodule.loadersに以下を記述
```
{
    test: /\.css$/,
    loaders: ['style-loader', 'css-	loader'],
}

Gulp, Stylus関連
```
$ yarn add gulp gulp-connect gulp-notify gulp-plumber gulp-stylus path --dev
yarn add v0.15.1
```


babel and react

```
$ npm i -D babel-core babel-loader babel-preset-es2017 babel-preset-react
```



webpackのエラー詳細を表示
```
$ ./node_modules/.bin/webpack --display-error-details
```