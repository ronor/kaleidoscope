# Docker


## コンテナとイメージの削除

コンテナ削除はrmを使う。
```
$ docker rm `docker ps -a -q`
```

イメージ全削除はrmiを使う。
```
$ docker rmi `docker images -q`
```

```
Error response from daemon: conflict: unable to delete 2100927ad00d (must be forced) - image is referenced in multiple repositories
```

上記のようなエラーで削除できないイメージは、以下のコマンドで削除。

```
$docker rmi `docker images | sed -ne '2,$p' -e 's/  */ /g' | awk '{print $1":"$2}'`
```