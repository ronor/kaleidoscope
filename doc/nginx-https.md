# Nginx


## SSL設定

Dockerコンテナのnginxに証明書を追加し、https接続するまでの設定。

```
$ mkdir nginx/keys
$ cd nginx/keys
$ openssl req -new -x509 -keyout server.key -out server.crt -nodes
```
でserver.keyとserver.crtを作成。パスフレーズがないものを作る。
次にnginx用のconfファイルを編集し、SSLの設定を行う。

```
$ vi nginx/conf.d/default.conf
```
```
server {
       listen 80;
       server_name localhost;
       return 301 https://$host$request_uri;
}

server {
       server_tokens off;

       listen       443 ssl;
       server_name  localhost;

       ssl on;
       ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
       ssl_certificate /etc/nginx/keys/server.crt;
       ssl_certificate_key /etc/nginx/keys/server.key;

       location / {
                proxy_pass http://app-server:3000;
       }

       location ~* \.(map|css|js|png|gif|jpg|pdf|xml|json|zip|gz|bz2|ico|woff|woff2)$ {
                root /usr/share/nginx/html;
                index index.html index.htm;
       }

       error_page   500 502 503 504  /50x.html;
       location = /50x.html {
                root   /usr/share/nginx/html;
       }
}
```

最後にDockerfileを編集し、confファイルとserver.key、server.crtをコンテナに追加する。
```
FROM nginx

ADD ./conf.d/default.conf /etc/nginx/conf.d/default.conf
RUN mkdir /etc/nginx/keys
ADD ./keys/server.crt /etc/nginx/keys/server.crt
ADD ./keys/server.key /etc/nginx/keys/server.key

RUN mkdir /root/logs
RUN chmod 400 /etc/nginx/keys/server.key

EXPOSE 433
CMD ["nginx", "-g", "daemon off;"]
```

## HTTP2

confファイルに以下のようにhttp2の指定を記述するとHTTP2になる。
```
listen 443 ssl http2;
```
## HTST
以下の内容をconfファイルに記述
```
add_header Strict-Transport-Security 'max-age=31536000; includeSubDomains;preload';
```

