# ring

## run-jetty

|||
|---|---|
|:port|待機するポート番号|
|:host|待機するホスト名|
|:ssl?|trueでhttps接続許可|
|:ssl-port|SSLのポート番号設定|
