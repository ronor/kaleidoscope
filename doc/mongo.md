# MongoDB with clojure on docker

## 認証

mongodb.confでauthを有効にしコンテナに追加。
entry.shというスクリプトでMongoDBのユーザー追加処理等を最初に行うようにする


コンテナを起動し、先程設定した管理者ユーザーで認証したあと、特定のDB用のユーザーを作成します。
```
$ docker exec -it mongodb /bin/bash
$ mongo
$ db.auth('root', 'password')
$ use dbname
$ db.createUser({
  user: "dbuser",
  pwd: "password",
  roles: [
    {
      role: {db: "dbname", role: "readWrite"}
    }
  ]
})
```

Clojureではmonger.credentialsを使います。

```
(ns project.core
  (:require 
  　〜省略〜
    [monger.core :as mg]
    [monger.collection :as mc]
    [monger.credentials :as mcred]))
    
(let [conn (mg/connect-with-credentials "db-server:27017" (mcred/create "dbuser" "dbname" "password"))
  db (mg/get-db conn "dbname")
  coll "collection_name"]
  (mc/find db coll {}))
```

