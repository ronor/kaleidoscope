# Note

CMS作る上でのメモ書き。

## Webの中身

- フロントページ
- 静的ページ
- カテゴリーページ
- 記事ページ
- ウィジェット
- 広告
- 


## 全体管理のためのデータ構造

### Collection:site
|Name|||
|---|---|---|
|_id|||
|サイト名|sitename||
|ロケール|locale|言語|
|タイムゾーン|timezone|Asia/Tokyo的な|
|メンテナンス|maintenance|1の場合メンテナンスモードになる|
|メンテナンスIP|maintenance_ip|メンテナンスモードでも閲覧可能なIPを指定|


## メニューのデータ構造

### Collection:menu
カテゴリーと静的ページのIdentifierを登録することでメニュー項目を追加する。ステータスにより、メニュー自体の有効化、無効化を行う。
登録されたカテゴリーや静的ページが非公開状態の場合、その要素は表示されない。

|Name|||
|---|---|---|
|_id|||
|識別子|identifier|メニュー識別用の英名|
|要素|elements|カテゴリーもしくは静的ページのidentifierと、カテゴリーか静的ページかを識別する値の組み合わせのマップ|
|ステータス|status|公開、非公開|


### データ例
```
{
  "_id": "xxxxxxxxxxxxxx",
  "identifier": "main-navigation",
  "elements": [
    {
      "type": "category",
      "label": "カテゴリー名1",
      "identifier": "category-identifier1"
      "children": [
      	"type": "category",
        "label": "カテゴリー名2",
        "identifier": "category-identifier2"
        "children": [
         ...
        ]
      ]
    },
    {
      "type": "static",
      "label": "静的ページ名",
      "identifier": "static-identifier"
    }
  ],
  "status": "public"
}
```

フロントページ（ホーム）へのリンクをメニューに追加するには、typeをcategoryにし、identifierをindex(固定)にする。indexは予約語で、これをカテゴリーの識別子として使うことはできない。
カテゴリーページは子カテゴリーも全てデータとして取得できる。これはサブメニューの構築等に利用することを想定する。

フロントカテゴリーを指定してメニューを登録した場合は、そのままサイトマップとして利用できるようにする（staticを別途追加していく必要はあるが……）。

テンプレート側では、メニューの識別子を指定することでこのデータを読み出し、メニューを構築することができる。
また、/api/menu/menu-identifierをfetchすることで、このデータをJSON形式で取得できる。

## カテゴリーのデータ構造

### Collection: category
初期状態でフロントカテゴリーが存在する。
フロントカテゴリーは単一の記事のみ属し、カテゴリーページを持たない。
ユーザーが変更することはできない。
また、ユーザーが追加する全てのカテゴリーページの親は、フロントカテゴリーに最終的に紐付く。

|Name|||
|---|---|---|
|_id|||
|識別子|identifier|URL等に使用されるカテゴリーの英名|
|名称|label|表示されるカテゴリーの名前|
|親カテゴリー|parent|単一。特定のカテゴリーを親として指定しない場合、フロントカテゴリーに紐付けられる|
|子カテゴリー|children|複数。マップで取り扱う|
|コンテナ|container|利用するcontainer|
|ステータス|status|カテゴリー単位での公開、非公開|


## テンプレートについて

### システム管理下にあるテンプレート
|テンプレートHTML|説明|
|---|---|
|container.html|下の各テンプレートをロードする親テンプレート|
|container-{identifier}.html|標準のcontainer.htmlとは別に用意したい場合に。category、static、article、frontで個別に設定可能|
|front.html|フロントページ専用テンプレート|
|category.html|カテゴリー一覧テンプレート|
|category-{category_identifier}.html|特定のカテゴリー記事一覧テンプレート|
|article.html|記事テンプレート|
|article-{category_identifier}.html|特定のカテゴリー内にある全ての記事に適用されるテンプレート|
|article-{category_identifier}-{article_identifier}.html|特定のカテゴリー内の特定の記事に適用されるテンプレート|
|static.html|静的ページテンプレート|
|static-{static_identifier}.html|特定の静的ページに適用されるテンプレート|

### システム管理下にないテンプレート

システム管理下にないテンプレートは、保守性の向上を目的にユーザーが独自に作成できる。これらは、システム管理下にあるテンプレートから手動で呼び出すことでページに適用される。


## 各デバイス向けのテンプレート

PC、スマホ、タブレットといったデバイス種別ベースでの切り替えを行う（画面サイズによる切り替えはCSSに任せる)。

- デバイス名
- デバイス判定処理
- デバイス該当時に利用するテンプレートディレクトリ
- 適応優先度

の4つのデータを組にして、利用するテンプレートを切り分ける。
※適応優先度はテンプレートファイル単位で設定できたほうがいいかもしれない。





## 静的ページのデータ構造


静的ページは、特定のテーマのもとで作成していく記事とは別に、独立したひとつのページを作成したい場合に使用する。
ブログでればプロフィールページ、会社であれば会社概要などがこのページに該当する。

### Collection:static

|Name|||
|---|---|---|
|_id|||
|識別子|identifier||
|名称|label||
|本文|contents||
|タグ|tags||
|作成日|created||
|公開日|published||
|更新日|updated||
|公開予約日|||
|コンテナ|container|利用するcontainer|
|ステータス|status|下書き、公開、非公開、削除、予約|

## 投稿記事のデータ構造

### Collection:article
|Name|||
|---|---|---|
|_id|||
|識別子|||
|タイトル|||
|本文|||
|タグ|||
|カテゴリー||属するカテゴリーID|
|作成日|||
|公開日|||
|更新日|||
|公開予約日|||
|コンテナ|container|利用するcontainer|
|ステータス|status|下書き、公開、非公開、削除、予約|



## データ拡張

ウェブページに追加したい様々なデータを統一的に取り扱う仕組みを提供する。

- アクセス数を保持したい
- OGPタグ、SEO用のタグ情報を追加したい
- 記事評価のデータを入れたい
- コメントデータを入れたい
- タグ設定を管理
- リダイレクト設定を追加したい

preprocess.cljは標準データにない拡張データを検査し、テンプレートに自動的にバインドする。拡張データのコレクション名はexpand_で始まる必要がある。
また、/api/expand/data-identiferをfetchすることで、そのデータを非同期で取り込むこともできる。

```
{
	"identifier": "xxxxxxxxxxxxxx",
    // 対象となるページと除外するページを設定
    "pages": {
       "include": [
         "index",
         "category"
       ],
       "exclude": [
         "static",
         "category-identifier"
       ]
    ],
    "data": {
      // 自由定義
    },
    "code": ""
}
```
include、excludeには、index、category、static、articleなどの予約された語句、ユーザーが設定したcategory-identifierなどの識別子を指定する。

バインドされたデータやfetchされたデータは、上記の全ての情報を含むため、テンプレートやフロント側で該当するidentifierの有無をチェックしてから利用する。

codeは、記述されたClojureのコードを評価した結果が入る。リクエストのデータ、DB、拡張データのdataにアクセスできるようにする（したい）。

## JS, CSSについて
特定ページでのみ読み込みたいJavascriptやCSSがある場合、データ拡張を利用する。

## コンテンツ検索

カテゴリー、タグ絞り込み、全文検索対応


## お問い合わせ、掲示板、その他データ登録のための仕組み

/api/register/register-identifier
に対してPOSTすることで、register-identifierの名前でコレクションが新たに作成される。

セキュリティ上の懸念がある。

- WebからのPOSTのみ受け付け
- 同一IPからの高速な連続のPOSTは弾く
- 


## Fetch API

HTMLレスポンスによる取得の他、全てのコンテンツは/api以下にアクセスすることでfetchできる。category, static, index, articleの全てをfetchできる。

|METHOD|POST|DESCRIPTION|PARAMETER|
|---|---|---|---|
|GET|/api/index|フロントページのデータ||
|GET|/api/categories|全てのカテゴリーデータ|
|GET|/api/category-identifier|特定のカテゴリーと、そこに紐付く全ての子カテゴリーデータ||
|GET|/api/category-identifier/articles|特定のカテゴリー以下の全ての記事データ||
|GET|/api/category-identifier/article-identifier|特定のカテゴリー以下の特定の記事データ||
|GET|/api/statics|静的ページ全てのデータ||
|GET|/api/static-identifier|特定の静的ページのデータ||
|GET|/api/menus|全てのメニューデータ||
|GET|/api/menu-identifier|特定のメニューデータ||
|GET|/api/register/register-identifier|register-identifierのデータ||


## 管理ツールについて

管理画面（Webのフロントエンドインターフェース）にこだわらない。
全ての操作はコマンドラインベースのツールとして作成し、Webインターフェースが必要な場合はWebAPIでラップ、クライアントアプリケーションが必要な場合は通信でコマンドを送る、といった形の設計にする。
コマンドラインベースのツールを基本にすることで、各種作業の自動化や、ベース部分の構築を自動化するといったことが簡単にできることを目指す。







## SEO

title
meta
keyword

Webmasterのタグ設定
Analyticsの設定

noindex, nofollowの設定

dns-prefetch
canonical


### 管理画面

/manager/admin - 管理画面（認証後のアクセスページ）
/manager/auth - 認証

"/manager"の部分は変更可能にする



### TODO
- 認証
- 投稿
- カテゴリー制御
- React
- SPA, PWA


