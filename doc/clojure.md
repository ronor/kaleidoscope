# Clojure

## Repl

### リロード

```
=> (use 'project-name.core :reload)
```

全てのネームスペースに渡ってreloadを行う場合、以下のツールを利用する。

[tools.namespace](https://github.com/clojure/tools.namespace)

```
[org.clojure/tools.namespace "0.2.11"]
```

```
user=> (require '[clojure.tools.namespace.repl :refer [refresh]])
nil

user=> (refresh)
:reloading (com.example.util com.example.app com.example.app-test)
:ok
```

## 静的解析ツール

[kibit](https://github.com/jonase/kibit)


## 認証

[buddy](https://github.com/funcool/buddy)

