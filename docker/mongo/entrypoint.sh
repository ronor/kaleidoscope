#!/bin/sh

set -e

ADMIN_USER=${MONGODB_USER:-admin}
ADMIN_PASSWORD=${MONGODB_PASSWORD:-password}
ADMIN_DATABASE=${MONGODB_DATABASE:-admin}

USER=${MONGODB_USER:-ks}
PASSWORD=${MONGODB_PASSWORD:-hogehoge}
DATABASE=${MONGODB_DATABASE:-kaleidoscope}

echo ${USER}
echo ${PASSWORD}
echo ${DATABASE}

if [ ! -f /data/db/storage.bson ]; then
  mongod --fork --nojournal --logpath /var/log/mongodb/setup.log
  mongo "$ADMIN_DATABASE" --eval "db.createUser({
      user: '$ADMIN_USER',
      pwd:  '$ADMIN_PASSWORD',
      roles: [{role:'dbAdminAnyDatabase', db: '$ADMIN_DATABASE'}]
    });
    db.auth('$ADMIN_USER', '$ADMIN_PASSWORD');
  "

  mongo "$DATABASE" --eval "db.createUser({
       user: '$USER',
       pwd: '$PASSWORD',
       roles: [{role: 'readWrite', db: '$DATABASE'}]
    });
  "
  mongo "$DATABASE" --eval "db.createCollection('site');
        db.createCollection('menus');
        db.createCollection('categories');
        db.createCollection('statics');
        db.createCollection('articles');
  "
  mongo "$DATABASE" --eval "db.site.insert({
            'sitename': 'Kaleidoscope',
            'locale': 'ja_JP.UTF-8',
            'timezone': 'Asia/Tokyo',
            'maintenance': 0,
            'maintenance_ip': ''
        });
        db.categories.insert({
            'identifier': 'index',
            'label': 'ホーム',
            'parent': '',
            'children': '',
            'container': 'container.html',
            'status': 1
        });
  "

  cat /var/log/mongodb/setup.log
  mongod --shutdown
  echo "Entrypoint added user '$USER' with password on database '$DATABASE'"
fi

exec "$@"
