(ns kaleidoscope.core
  (:require [ring.adapter.jetty :as server]
            [ring.util.response :as res]
            [ring.util.response :refer [redirect]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [compojure.core :refer :all]
;            [compojure.core :refer [defroutes context GET]]
            [compojure.route :as route]
            [selmer.parser :as parser]
            [monger.core :as mg]
            [monger.collection :as mc]
            [monger.credentials :as mcred]
            [clojure.data.json :as json]
            [cheshire.core :refer :all]
            [buddy.auth :refer [authenticated? throw-unauthorized]]
            [buddy.auth.backends.session :refer [session-backend]]
            [buddy.auth.middleware :refer [wrap-authentication
                                           wrap-authorization]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [clj-html-compressor.core :as comp])
  (:import [com.mongodb MongoOptions ServerAddress])
  (:import [org.bson.types.ObjectId]))


(def manager
  "管理画面の設定"
  {:base-path "/manager"
   :anti-forgery-token *anti-forgery-token*})

(def manager-path
  "管理画面のベースとなるパス"
  (get manager :base-path))

(def admin-path
  "管理画面ログイン後のパス"
  (str (get manager :base-path) "/admin"))

(def auth-path
  "管理画面ログイン前のパス(Login, Logout)"
  (str (get manager :base-path) "/auth"))


(load "core_config")

(defn insert-test []
  (let [conn (mg/connect {:host "db-server" :port 27017})
        db   (mg/get-db conn "kaleidoscope")]
    (mc/insert db "documents" { :data "abcdefg" })))

(defonce server (atom nil))
(parser/set-resource-path! (clojure.java.io/resource "templates"))

(defn html-not-found [res]
  (res/content-type res "text/html; charset=utf-8")
  (res/status res 404))

(defn html [res]
  (res/content-type res "text/html; charset=utf-8"))

(defn not-found-view [req]
  '("<h1>Not found</h1>"))

(defn not-found [req]
  (-> (not-found-view req)
      res/response
      html))

(defn home-view [req]
  (parser/render-file "container.html"
                      {:page (parser/render-file
                              "home.html" {})}))

(defn home [req]
  (-> (home-view req)
      res/response
      html))

(defn category-view [name]
  (println name)
  (parser/render-file (str name ".html") {}))

(defn category [name]
  (-> (category-view name)
      res/response
      html))

(defn get-menu-data []
  "DBからメニュー定義を取得"
  (let [conn (mg/connect-with-credentials "db-server:27017"
              ;; (create username database password)
              (mcred/create
               "ks" "kaleidoscope" "hogehoge"))
        db   (mg/get-db conn "kaleidoscope")
        coll "categories"]
    (mc/find-one-as-map db coll {:name "hogehogehoge"})))

; mc/find - return dbcursor
; mc/find-one-as-map - return clojure maps

(defn print-menu-data []
  (let [list (get-menu-data)]
    ;(print list)))
    (generate-string (dissoc list :_id) {:pretty true})))
    ;(generate-string (map #(get % "child") list) {:pretty true})))

(defn fetch-view [req]
  (print-menu-data))

(defn fetch [req]
  (-> (fetch-view req)
      res/response
      html))

(defn get-template-name [name]
  (if (empty? name) "home.html" (str name ".html")))

(defn get-template [name]
  (parser/render-file (get-template-name name)
                      (if (empty? name)
                        {:items (range 200)}
                        {})))


(defn get-data [name]
  (if (empty? name)
    (merge {:page (get-template name)}
           {:title "title"}
           ogp-resources))
    (merge {:page (get-template name)}
           {:title "title"}
           ogp-resources))

(defn show-page [name]
  (selmer.filters/add-filter! :embiginate clojure.string/upper-case)
  (selmer.filters/add-filter! :not-empty-last (fn [x] (not-empty (last x))))
  (try
    (-> (comp/compress (parser/render-file "container.html" (get-data name))
                       {:enabled false})
        res/response html)
    (catch Exception e {:status 404
                        :headers {"Content-Type" "text/html"}
                        :body (parser/render-file "container.html" (get-data "not-found"))})))




(defn get-admin-data [name]
  "管理画面表示用データ取得"
  (merge {:page (get-template name)}
         {:manager-path manager-path}
         {:base-path admin-path}
         {:auth-path auth-path}))

(defn show-admin-page [name req]
  "管理画面表示"
;;  (when (not (authenticated? req))
;;    (throw-unauthorized (redirect (str admin-path "/login"))))
  (-> (parser/render-file (str admin-path "/container.html")
                          (merge (get-admin-data (str admin-path "/" name)) {:path name}))
      (res/response) html))

(defn show-login-page []
  "管理画面ログイン"
  (-> (parser/render-file (str auth-path "/login.html") {:manager-path manager-path
                                                         :anti-forgery-token *anti-forgery-token*
                                                         :base-path auth-path})
      (res/response) html))

(defn post-login [req]
  (let [user-id (get-in req [:form-params "user-id"])]
    (-> (redirect (get-in req [:query-params "next"] (str admin-path "/system")))
        (assoc-in [:session :identity] (keyword user-id)))))

(defn get-logout []
  (-> (redirect (str admin-path "/login"))
      (assoc :session {})))

(defroutes handler
  (GET "/" [] (show-page ""))
  (GET ["/category/:name" :name #".*"] [name] (category name))
  (GET "/fetch" [] fetch)

  (GET (str auth-path "/login") [] (show-login-page))
  (POST (str auth-path "/login") req (post-login req))
  (GET (str auth-path "/logout") [] (get-logout))

  (GET admin-path [] (show-admin-page ""))
  (GET [(str admin-path "/:page") :page #".*"] [page req] (show-admin-page page req))

  (GET ["/:page" :page #".*"] [page] (show-page page))
  (route/not-found "<h1>not found</h1>"))

(defn unauthorized-handler [req meta]
  (if (authenticated? req)
    (redirect (str auth-path "/system")
    (redirect (format (str admin-path "/login?next=%s")) (:uri req)))))

(def app
  (let [rules [{:pattern #"^/manager/admin/.*"
                :handler authenticated?
                :redirect (str auth-path "/login")}]
        backend (session-backend {:unauthorized-handler unauthorized-handler})]
    (-> handler
        (wrap-access-rules {:rules rules :policy :allow})
        (wrap-authentication backend)
        (wrap-authorization backend)
        (wrap-defaults site-defaults))))

(def reloadable-app
  "appを再読込"
  (wrap-reload #'app))
;;  (wrap-reload #'handler))

(defn start-server []
  (when-not @server
    (reset! server (server/run-jetty reloadable-app
                                     {:port 3000
                                      :host "app-server"
                                      :join? false}))))

(defn stop-server []
  (when @server
    (.stop @server)
    (reset! server nil)))

(defn restart-server []
  (when @server
    (stop-server)
    (start-server)))

(defn -main []
  (start-server))
