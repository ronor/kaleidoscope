const path = require('path');
const src = path.resolve(__dirname, 'src/js');
const dist = path.resolve(__dirname, '../../public/manager/js');



module.exports = {
  entry: {
    "app": src + '/app.jsx'
    //"index": src + '/index.js'
  },
  output: {
    path: dist,
    filename: '[name].js'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      {
        test: /\.js(x?)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        loaders: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              url: false
            }
          }
        ]
      },
    ]
  },
};
