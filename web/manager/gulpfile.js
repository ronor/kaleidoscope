let gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    connect = require('gulp-connect');

gulp.task('stylus', () => {
    gulp.src('src/stylus/**/*.styl')
    .pipe(stylus())
    .pipe(gulp.dest('../public/'));
});

gulp.task('watch', () => {
  gulp.watch('src/stylus/**/*.styl', ['stylus']);
});
