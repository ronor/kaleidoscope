import React from 'react';
import ReactDOM from 'react-dom';
import {Button} from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';

class HelloReact extends React.Component {
  render() {
    return(
      <div>
        <Button color="danger">Danger!</Button>
      </div>);
  }
}

ReactDOM.render(
  <HelloReact />,
  document.querySelector(".content")
);
