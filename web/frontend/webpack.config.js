const path = require('path'),
      webpack = require('webpack'),
      src = path.resolve(__dirname, 'src/js'),
      dist = path.resolve(__dirname, '../../public/js')


module.exports = {
  entry: {
    "app": src + '/app.js',
    "kaleidoscope": src + '/kaleidoscope.js'
  },
  output: {
    path: dist,
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.tag$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: [
          {
            loader: 'riotjs-loader',
          }
        ]
      },
      {
        test: /\.js[x]?$|\.tag$/,
        enforce: 'post',
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.tag']
  },
  plugins: [
    new webpack.ProvidePlugin({
      riot: 'riot'
    })
  ]
}
