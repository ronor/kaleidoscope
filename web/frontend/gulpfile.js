let gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    connect = require('gulp-connect'),
    autoprefixer = require('gulp-autoprefixer')

let stylus_target = [
  'reset',
  'style',
//  'base',
//  'main',
];

gulp.task('build:stylus', () => {
  for (let i = 0; i < stylus_target.length; i++) {
    console.log('target:' + stylus_target[i] + '.styl')
    gulp.src('./src/stylus/**/' + stylus_target[i] + '.styl')
      .pipe(plumber({
        handleError: (error) => {
          console.log(error)
          this.emit('end')
        }
      }))
      .pipe(stylus({
        compress: false
      }))
      .pipe(autoprefixer({
        browsers: ['last 1 versions'],
        cascade: false
      }))
      .pipe(gulp.dest('../../public/'))
  }
});

gulp.task('watch', () => {
  gulp.watch('./src/stylus/**/*.styl', ['build:stylus'])
})
